import 'dart:core';

void main(){
    // dynamic name = "ahmad";
    // name = 2;

    // List<Object> name = [4,"angah"];
    // bool abu = true;
    //
    // Map<String, dynamic> person = {
    //     "name": "Ahmad",
    //     "age": 20,
    //     "gender": "male"
    // };
    //
    // print(person["name"]);

    List<String> nameList = ["ahmad", "ali", "abu"];

    List <Map<String, dynamic>> studentList = [
        {"name": "abu", "age": 12},
        {"name": "angah", "age": 34},
        {"name": "ali", "age": 40},
    ];


    printDetails(String name, int age){
        print(name);
        print(age);
        print("The student name is $name");
        print("His Age is $age");
    }

    // studentList.forEach(printDetails);
    studentList.forEach((dynamic haha){
        printDetails(haha["name"] as String, haha["age"] as int);

    });


    // 1
    // printName(int haha){
    //     // print("Ahmad");
    //     print(nameList[haha]);
    // }
    //
    // printName(0);
    // printName(1);
    // printName(2);

    // 2
    // printName(String index){
    //     print(index);
    // }
    //
    // printName(nameList[0]);
    // printName(nameList[1]);
    // printName(nameList[2]);

    // 3
    // printName(String index){
    //     print(index);
    // }
    //
    // nameList.forEach(printName);


    // 4  
    // printName(String index){
    //     print(index);
    // }
    //
    // nameList.forEach((e) {
    //     print(e);
    // });
      

}
